require('dotenv').config()

const Discord = require('discord.js')
const client = new Discord.Client()
const forever = require('forever-monitor');

const offlineSince = {}
const afkSince = {}

var bot_token = process.env.BOT_TOKEN
console.log("bot_token: ", bot_token)
//var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080
//var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'
//
//server.listen(server_port, server_ip_address, function () {
//  console.log( "Listening on " + server_ip_address + ", port " + server_port )
//})

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`)
})

const findUserByName = function(name) {
  return client.users.find(user => user.username.toLowerCase().indexOf(name.toLowerCase()) !== -1)
}

const findUserById = function(uid) {
  return client.users.find(user => user.id === uid)
}

const time = function(now, then) {
  var delta = Math.abs(then - now) / 1000;
  var days = Math.floor(delta / 86400);
  delta -= days * 86400;

  var hours = Math.floor(delta / 3600) % 24;
  delta -= hours * 3600;

  var minutes = Math.floor(delta / 60) % 60;
  delta -= minutes * 60;
  var seconds = Math.floor(delta % 60);  // in theory the modulus is not required

  return days + 'd ' + hours + 'h ' + minutes + 'm ' + seconds + 's'
}

client.on("message", function(msg) {
  if (msg.content.indexOf( '-whois') === 0 &&  msg.content.split(' ').length === 2) {
//    await msg.delete()
    let userId = msg.content.split(' ')[1].replace(/[<@!>]/g, '')
    let user;
    if (isNaN(parseInt(userId, 10))) {
      user = findUserByName(userId)
    } else {
      user = findUserById(userId)
    }

    userId = user ? user.id : null

    if (userId) {
      let responseMsg = ''

      if (offlineSince[userId] > 0) {
        const offlineTime = Date.now() - offlineSince[userId]
        responseMsg += 'Offline since: ' + time(Date.now(), offlineSince[userId])
        if (offlineTime > client.uptime - 10000) {
          responseMsg += ' (Offline since bot connected!)'
        }
      } else if (afkSince[userId] > 0) {
        const afkTime = Date.now() - afkSince[userId]
        responseMsg += 'Away/DnD since: ' + time(Date.now(), afkSince[userId])
        if (afkTime > client.uptime - 10000) {
          responseMsg += ' (AFK since bot connected!)'
        }
      } else {
        responseMsg += 'User online'
      }

      msg.channel.send('['+ user.username + '] - ' + responseMsg)
    } else {
      msg.channel.send('No user info for: ' + msg.content.split(' ')[1])
    }
  }
})

client.setInterval(function () {
  console.log('polling...', time(0, client.uptime))
//  client.users.filter(user => !user.bot).forEach(function (user) {
  client.users.forEach(function (user) {
    const precense = user.presence
    const uid = user.id

    if (precense.status !== 'online') {
      if (!afkSince[uid]) afkSince[uid] = Date.now()
    } else {
      afkSince[uid] = false
    }

    if (precense.status === 'offline') {
      if (!offlineSince[uid]) offlineSince[uid] = Date.now()
    } else {
      offlineSince[uid] = false
    }
  })
}, 1000)

client.on('disconnect', (e) => {
  console.log('DISCONNECTED:', e)

  setTimeout(function () {
    console.log('Retrying...')
    client.login(bot_token)
  }, 1000)
})

client.login(bot_token)